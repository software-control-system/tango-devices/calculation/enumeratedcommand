package fr.soleil.tango.server.enumcommand;

import org.tango.server.StateMachineBehavior;
import org.tango.server.command.CommandConfiguration;
import org.tango.server.command.ICommandBehavior;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.clientapi.TangoCommand;

public final class DynamicEnumCommand implements ICommandBehavior {

    private final TangoCommand command;
    private final Object argin;
    private final CommandConfiguration config = new CommandConfiguration();

    public DynamicEnumCommand(final String commandName, final TangoCommand command, final Object argin) {
	this.command = command;
	this.argin = argin;
	config.setName(commandName);
    }

    @Override
    public CommandConfiguration getConfiguration() throws DevFailed {
	return config;
    }

    @Override
    public Object execute(final Object arg) throws DevFailed {
	command.execute(argin);
	return null;
    }

    @Override
    public StateMachineBehavior getStateMachine() throws DevFailed {
	return null;
    }

}
