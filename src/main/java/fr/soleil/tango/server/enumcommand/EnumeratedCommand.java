package fr.soleil.tango.server.enumcommand;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import org.apache.commons.lang3.ArrayUtils;
import org.tango.server.InvocationContext;
import org.tango.server.ServerManager;
import org.tango.server.annotation.AroundInvoke;
import org.tango.server.annotation.Attribute;
import org.tango.server.annotation.Delete;
import org.tango.server.annotation.Device;
import org.tango.server.annotation.DeviceProperty;
import org.tango.server.annotation.DynamicManagement;
import org.tango.server.annotation.Init;
import org.tango.server.annotation.State;
import org.tango.server.annotation.Status;
import org.tango.server.annotation.TransactionType;
import org.tango.server.dynamic.DynamicManager;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.soleil.tango.clientapi.TangoCommand;

/**
 * Class Description: This device predefines an enumerated list of values for a simple scalar attribute. Its tango
 * interface is dynamically created from the properties attributeLabelList and attributeEnumeratedValues.
 *
 * The dynamically created attributes are boolean. For example : EnumeratedLabelList="isInserted", "isExtracted"
 * EnumeratedValueList = "0","100" Then 2 attributes are created : isInserted and isExtracted. When isInserted is set to
 * true then the controlled attribute is set to 0 When isExtracted is set to true then the controlled attribute is set
 * to 100
 *
 * @author ABEILLE
 *
 */
@Device(transactionType = TransactionType.NONE)
public final class EnumeratedCommand {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    /**
     * The label of the created command
     */
    @DeviceProperty
    private String[] enumeratedLabelList = new String[0];
    /**
     * The enumerated values for corresponding labels defined in EnumeratedLabelList
     */
    @DeviceProperty
    private String[] enumeratedValueList = new String[0];
    /**
     * The name of the controlled command
     */
    @DeviceProperty
    private String commandName = "";

    @Attribute
    private String executedCommand = "";

    @Attribute
    private String executedTimeStamp = "";

    @State
    private DevState state = DevState.OFF;

    @Status
    private String status = "enum command OK";

    @DynamicManagement
    private DynamicManager dynamicManager;

    private TangoCommand command;

    public void setDynamicManager(final DynamicManager dynamicManager) {
        this.dynamicManager = dynamicManager;
    }

    public void setEnumeratedLabelList(final String[] enumeratedLabelList) throws DevFailed {
        if (enumeratedLabelList.length == 0) {
            DevFailedUtils.throwDevFailed("EnumeratedLabelList is empty");
        }
        this.enumeratedLabelList = Arrays.copyOf(enumeratedLabelList, enumeratedLabelList.length);
    }

    public void setEnumeratedValueList(final String[] enumeratedValueList) throws DevFailed {
        if (enumeratedValueList.length == 0) {
            DevFailedUtils.throwDevFailed("enumeratedValueList is empty");
        }
        this.enumeratedValueList = Arrays.copyOf(enumeratedValueList, enumeratedValueList.length);
    }

    public void setCommandName(final String commandName) throws DevFailed {
        if (commandName.equals("")) {
            DevFailedUtils.throwDevFailed("commandName must be defined in device property");
        }
        this.commandName = commandName;
    }

    @Delete
    public void deleteDevice() throws DevFailed {
        dynamicManager.clearAll();
    }

    @Init
    public void initDevice() throws DevFailed {
        if (enumeratedLabelList.length != enumeratedValueList.length) {
            DevFailedUtils.throwDevFailed("enumeratedLabelList and enumeratedValueList must have same sizes");
        }

        command = new TangoCommand(commandName);
        createComand();
    }

    /**
     * This return true is the creation of attribute is possible and create the attributes
     */
    private void createComand() throws DevFailed {
        for (int i = 0; i < enumeratedLabelList.length; i++) {
            final String tmpCommandName = enumeratedLabelList[i].trim();
            final String tmpValue = enumeratedValueList[i].trim();
            final DynamicEnumCommand cmd = new DynamicEnumCommand(tmpCommandName, command, tmpValue);
            dynamicManager.addCommand(cmd);
        }
    }

    /**
     * @param ctx
     * @throws DevFailed
     */
    @AroundInvoke
    public void aroundInvoke(final InvocationContext ctx) throws DevFailed {
        final String enumName = ctx.getNames()[0];
        switch (ctx.getContext()) {
            case PRE_COMMAND:
                if (ArrayUtils.contains(enumeratedLabelList, enumName)) {
                    executedTimeStamp = DATE_FORMAT.format(new Date());
                    executedCommand = enumName;
                }
                break;
            default:
                break;
        }
    }

    public DevState getState() throws DevFailed {
        state = command.getDeviceProxy().state();
        return state;
    }

    public void setState(final DevState state) {
        this.state = state;
    }

    public String getStatus() throws DevFailed {
        status = command.getDeviceProxy().status();
        return status;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    public static void main(final String[] args) {
        ServerManager.getInstance().start(args, EnumeratedCommand.class);
    }

    public String getExecutedCommand() {
        return executedCommand;
    }

    public String getExecutedTimeStamp() {
        return executedTimeStamp;
    }

}
